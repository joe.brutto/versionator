package utility

import "io"

// CloseQuietly swallows any errors related to closing a particular object.
func CloseQuietly(closer io.Closer) {
	// TODO: this is bad practice and we should add some error handling
	_ = closer.Close()
}
