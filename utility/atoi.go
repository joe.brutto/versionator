package utility

import "strconv"

// MustToUint converts a string to a unit assuming that it has to be done (or panic).
func MustToUint(input string) uint {
	asInt, err := strconv.Atoi(input)
	if err != nil {
		panic(err)
	}
	return uint(asInt)
}
