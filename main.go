package main

import (
	"github.com/alecthomas/kong"
	"gitlab.com/joe.brutto/versionator/cli"
)

func main() {
	ctx := kong.Parse(&cli.Cli{})
	err := ctx.Run()
	ctx.FatalIfErrorf(err)
}
