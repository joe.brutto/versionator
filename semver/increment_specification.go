package semver

import (
	"fmt"
	"strings"
)

const (
	IncrementSpecificationMajor = IncrementSpecification("major")
	IncrementSpecificationMinor = IncrementSpecification("minor")
	IncrementSpecificationPatch = IncrementSpecification("patch")
)

// IncrementSpecification type we use for specifications of increment by user-provided string.
type IncrementSpecification string

// IncrementSpecificationFor loads a specification based on the given input string.
func IncrementSpecificationFor(input string) IncrementSpecification {
	var loaded IncrementSpecification
	switch strings.TrimSpace(strings.ToLower(input)) {
	case string(IncrementSpecificationMajor):
		loaded = IncrementSpecificationMajor
	case string(IncrementSpecificationMinor):
		loaded = IncrementSpecificationMinor
	case string(IncrementSpecificationPatch):
		loaded = IncrementSpecificationPatch
	default:
		panic(fmt.Errorf("invalid increment specification (%s)", input))
	}
	return loaded
}
