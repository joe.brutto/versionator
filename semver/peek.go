package semver

import "fmt"

// Peek performs a simple peek to stdout for the data that is in the given SemanticVersion. If we are in "quiet mode"
// we simply take the data and verify things can be done without pushing information to stdout. It sounds useless, but
// it can be used for basic file validation to ensure that a valid Semantic Version is persisted somewhere.
func Peek(toPeek *SemanticVersion, incrementSpecification IncrementSpecification, quiet bool) {
	// data lookup
	original := toPeek.Format()
	toPeek.Increment(incrementSpecification)
	peeked := toPeek.Format()

	// format and display
	if !quiet {
		fmt.Printf("\033[92mCurrent Version:\u001B[0m  %s\n", original)
		fmt.Printf("\u001B[96mNext Version:\u001B[0m     %s\n", peeked)
	}
}
