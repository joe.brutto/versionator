package semver

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestValid ensures that we are properly handling all supported properties of a Semantic Version.
func TestValid(t *testing.T) {
	invalid := []string{
		"1",
		"1.0",
		"abc123",
		"v1.2.3",
	}

	valid := []string{
		"1.0.0",
		"1.9.0",
		"1.10.0",
		"1.11.0",
		"1.0.0-alpha",
		"1.0.0-alpha.1",
		"1.0.0-alpha.beta",
		"1.0.0-beta",
		"1.0.0-beta.2",
		"1.0.0-beta.11",
		"1.0.0-rc.1",
		"1.0.0-0.3.7",
		"1.0.0-x.7.z.92",
		"1.0.0-alpha+001",
		"1.0.0+20130313144700",
		"1.0.0-beta+exp.sha.5114f85",
	}

	for _, invalidInput := range invalid {
		assert.Falsef(t, Valid(invalidInput), "input \"%s\" should be an invalid Semantic Version", invalidInput)
	}

	for _, validInput := range valid {
		assert.Truef(t, Valid(validInput), "input \"%s\" should be a valid Semantic Version", validInput)
	}
}

// TestParse ensures we are properly parsing Semantic Version data into our internal object type(s).
func TestParse(t *testing.T) {
	invalid := []string{
		"1",
		"1.0",
		"abc123",
		"v1.2.3",
	}

	valid := map[string]string{
		"1.0.0":                      "Major[1]:Minor[0]:Patch[0]:PreRelease[]:BuildMetadata[]",
		"1.9.0":                      "Major[1]:Minor[9]:Patch[0]:PreRelease[]:BuildMetadata[]",
		"1.10.0":                     "Major[1]:Minor[10]:Patch[0]:PreRelease[]:BuildMetadata[]",
		"1.11.0":                     "Major[1]:Minor[11]:Patch[0]:PreRelease[]:BuildMetadata[]",
		"1.0.0-alpha":                "Major[1]:Minor[0]:Patch[0]:PreRelease[alpha]:BuildMetadata[]",
		"1.0.0-alpha.1":              "Major[1]:Minor[0]:Patch[0]:PreRelease[alpha.1]:BuildMetadata[]",
		"1.0.0-alpha.beta":           "Major[1]:Minor[0]:Patch[0]:PreRelease[alpha.beta]:BuildMetadata[]",
		"1.0.0-beta":                 "Major[1]:Minor[0]:Patch[0]:PreRelease[beta]:BuildMetadata[]",
		"1.0.0-beta.2":               "Major[1]:Minor[0]:Patch[0]:PreRelease[beta.2]:BuildMetadata[]",
		"1.0.0-beta.11":              "Major[1]:Minor[0]:Patch[0]:PreRelease[beta.11]:BuildMetadata[]",
		"1.0.0-rc.1":                 "Major[1]:Minor[0]:Patch[0]:PreRelease[rc.1]:BuildMetadata[]",
		"1.0.0-0.3.7":                "Major[1]:Minor[0]:Patch[0]:PreRelease[0.3.7]:BuildMetadata[]",
		"1.0.0-x.7.z.92":             "Major[1]:Minor[0]:Patch[0]:PreRelease[x.7.z.92]:BuildMetadata[]",
		"1.0.0-alpha+001":            "Major[1]:Minor[0]:Patch[0]:PreRelease[alpha]:BuildMetadata[001]",
		"1.0.0+20130313144700":       "Major[1]:Minor[0]:Patch[0]:PreRelease[]:BuildMetadata[20130313144700]",
		"1.0.0-beta+exp.sha.5114f85": "Major[1]:Minor[0]:Patch[0]:PreRelease[beta]:BuildMetadata[exp.sha.5114f85]",
	}

	for _, invalidInput := range invalid {
		_, err := Parse(invalidInput)
		assert.NotNilf(t, err, "\"%s\" is invalid an should return an error", invalidInput)
	}

	for validInput, expectedStringConversion := range valid {
		semver, err := Parse(validInput)
		assert.Nilf(t, err, "\"%s\" should be a valid Semantic Version", validInput)
		assert.NotNilf(t, semver, "\"%s\" gave us back a nil parsed Semantic Version somehow", validInput)
		assert.Equal(t, expectedStringConversion, semver.String(), "Invalid data conversion")
		assert.Equal(t, validInput, semver.Format(), "\"%s\" was not restored to its appropriate form", validInput)
	}
}

// TestMustParse ensures that we deal with panic properly.
func TestMustParse(t *testing.T) {
	assert.Panics(t, func() {
		MustParse("abc123")
	}, "should have a panic on \"abc123\"")
}

// TestParseTextFile tests basic parsing of simple text files.
func TestParseTextFile(t *testing.T) {
	// valid file
	semver, err := ParseTextFile("../.test-data/valid/valid-000.txt")
	assert.Nil(t, err, "Should not have an error on valid file (valid-000.txt)")
	assert.NotNil(t, semver, "Did not get back a value after parsing file (valid-000.txt)")

	// invalid file
	semver, err = ParseTextFile("../.test-data/invalid/invalid-000.txt")
	assert.NotNil(t, err, "Invalid data in file should result in an error (invalid-000.txt")
	assert.Nil(t, semver, "Invalid data should not return a parsed Semantic Version (invalid-000.txt)")

	// file that doesn't exist
	semver, err = ParseTextFile("should-not-exist-ever")
	assert.NotNil(t, err, "File that does not exist should result in error")
	assert.Nil(t, semver, "File that does not exist should not return a parsed Semantic Version")
}
