package semver

import (
	"bufio"
	"fmt"
	"gitlab.com/joe.brutto/versionator/utility"
	"os"
	"regexp"
	"strings"
)

const Regex = "^(?P<major>0|[1-9]\\d*)\\.(?P<minor>0|[1-9]\\d*)\\.(?P<patch>0|[1-9]\\d*)(?:-(?P<prerelease>(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$"

// Valid determines whether a given string appears to confirm to our support Semantic Version formatting.
func Valid(input string) bool {
	return regexp.MustCompile(Regex).Match([]byte(input))
}

// Parse requests that we parse in a Semantic Version based on our standard regular expression.
func Parse(input string) (*SemanticVersion, error) {
	// test validity
	if !Valid(input) {
		return nil, fmt.Errorf("\"%s\" is not a valid Semantic Version", input)
	}

	// actual parsing
	regex := regexp.MustCompile(Regex)
	matches := regex.FindStringSubmatch(input)

	semver := &SemanticVersion{}
	semver.Major = utility.MustToUint(matches[regex.SubexpIndex("major")])
	semver.Minor = utility.MustToUint(matches[regex.SubexpIndex("minor")])
	semver.Patch = utility.MustToUint(matches[regex.SubexpIndex("patch")])
	semver.PreRelease = matches[regex.SubexpIndex("prerelease")]
	semver.BuildMetadata = matches[regex.SubexpIndex("buildmetadata")]

	// exit
	return semver, nil
}

// MustParse is the same as the Parse function but will panic on error.
func MustParse(input string) *SemanticVersion {
	parsed, err := Parse(input)
	if err != nil {
		panic(err)
	}
	return parsed
}

// ParseTextFile attempts to parse the data from a simple text file.
func ParseTextFile(filePath string) (*SemanticVersion, error) {
	// read the data from file
	file, err := os.OpenFile(filePath, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer utility.CloseQuietly(file)

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	content := strings.TrimSpace(scanner.Text())

	// parse the data
	return Parse(content)
}
