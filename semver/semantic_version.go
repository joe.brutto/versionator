package semver

import (
	"fmt"
	"strings"
)

// SemanticVersion represents the primary data we are working with in its broken-out/parsed form. All supported constructs of
// a Semantic Version must be included here, and we expect the basic standards to be adhered to based on the currently
// support version of the Semantic Version specification that we are adhering to.
type SemanticVersion struct {
	Major         uint
	Minor         uint
	Patch         uint
	PreRelease    string
	BuildMetadata string
}

// String provides a custom string conversion for SemanticVersion objects for basic processing and debugging. This is
// not intended to be used as the conversion for actual data (a separate function has been provided for that) and is
// here for convenience purposes during testing.
func (s *SemanticVersion) String() string {
	return fmt.Sprintf("Major[%d]:Minor[%d]:Patch[%d]:PreRelease[%s]:BuildMetadata[%s]", s.Major, s.Minor, s.Patch, s.PreRelease, s.BuildMetadata)
}

// Format converts the SemanticVersion to a properly formatted Semantic Version specification for actual output.
func (s *SemanticVersion) Format() string {
	formatted := fmt.Sprintf("%d.%d.%d", s.Major, s.Minor, s.Patch)

	trimmedPreRelease := strings.TrimSpace(s.PreRelease)
	if trimmedPreRelease != "" {
		formatted += fmt.Sprintf("-%s", trimmedPreRelease)
	}

	trimmedBuild := strings.TrimSpace(s.BuildMetadata)
	if trimmedBuild != "" {
		formatted += fmt.Sprintf("+%s", trimmedBuild)
	}

	return formatted
}

// Equal determined whether this SemanticVersion is equal to another one ("deep").
func (s *SemanticVersion) Equal(s2 *SemanticVersion) bool {
	return s.Major == s2.Major &&
		s.Minor == s2.Minor &&
		s.Patch == s2.Patch &&
		s.PreRelease == s2.PreRelease &&
		s.BuildMetadata == s2.BuildMetadata
}

// IncrementMajor increments the major number for a Semantic Version and sets the minor and patch to zero.
func (s *SemanticVersion) IncrementMajor() {
	s.Major += 1
	s.Minor = 0
	s.Patch = 0
}

// IncrementMinor increments the minor number for a Semantic Version and sets the patch to zero.
func (s *SemanticVersion) IncrementMinor() {
	s.Minor += 1
	s.Patch = 0
}

// IncrementPatch increments the patch number for a Semantic Version.
func (s *SemanticVersion) IncrementPatch() {
	s.Patch += 1
}

// Increment takes a given IncrementSpecification and updates the appropriate field.
func (s *SemanticVersion) Increment(specification IncrementSpecification) {
	switch specification {
	case IncrementSpecificationMajor:
		s.IncrementMajor()
	case IncrementSpecificationMinor:
		s.IncrementMinor()
	case IncrementSpecificationPatch:
		s.IncrementPatch()
	default:
		panic(fmt.Errorf("invalid specification for increment (%s)", specification))
	}
}
