package semver

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestSemanticVersion_Equal tests our equality checks based on basic standards that we need.
func TestSemanticVersion_Equal(t *testing.T) {
	type TestTuple struct {
		Input1      string
		Input2      string
		ShouldEqual bool
	}

	toCheck := []TestTuple{
		{ Input1: "0.0.1", Input2: "0.0.1", ShouldEqual: true },
		{ Input1: "0.0.1", Input2: "1.0.0", ShouldEqual: false },
		{ Input1: "0.0.1", Input2: "0.1.0", ShouldEqual: false },
		{ Input1: "0.0.1", Input2: "0.0.2", ShouldEqual: false },
		{ Input1: "0.0.1-alpha", Input2: "0.0.1-alpha", ShouldEqual: true },
		{ Input1: "0.0.1", Input2: "0.0.1-alpha", ShouldEqual: false },
	}

	for _, tuple := range toCheck {
		semver1 := MustParse(tuple.Input1)
		semver2 := MustParse(tuple.Input2)
		assert.Equalf(t, tuple.ShouldEqual, semver1.Equal(semver2), "Invalid match on \"%s\" vs \"%s\"", tuple.Input1, tuple.Input2)
	}
}

// TestSemanticVersion_IncrementMajor tests our incrementing of major version data.
func TestSemanticVersion_IncrementMajor(t *testing.T) {
	valid := map[string]string{
		"0.0.1": "1.0.0",
		"0.1.1": "1.0.0",
		"1.1.1": "2.0.0",
		"1.1.1-alpha.1": "2.0.0-alpha.1",
		"1.1.1-alpha.1+build.123": "2.0.0-alpha.1+build.123",
	}

	for original, expected := range valid {
		semver := MustParse(original)
		semver.IncrementMajor()
		assert.Equal(t, expected, semver.Format(), "Invalid MAJOR version increment")
	}
}

// TestSemanticVersion_IncrementMinor tests our incrementing of minor version data.
func TestSemanticVersion_IncrementMinor(t *testing.T) {
	valid := map[string]string{
		"0.0.1": "0.1.0",
		"0.1.1": "0.2.0",
		"1.1.1": "1.2.0",
		"1.1.1-alpha.1": "1.2.0-alpha.1",
		"1.1.1-alpha.1+build.123": "1.2.0-alpha.1+build.123",
	}

	for original, expected := range valid {
		semver := MustParse(original)
		semver.IncrementMinor()
		assert.Equal(t, expected, semver.Format(), "Invalid MINOR version increment")
	}
}

// TestSemanticVersion_IncrementPatch tests our incrementing of patch version data.
func TestSemanticVersion_IncrementPatch(t *testing.T) {
	valid := map[string]string{
		"0.0.1": "0.0.2",
		"0.1.1": "0.1.2",
		"1.1.1": "1.1.2",
		"1.1.1-alpha.1": "1.1.2-alpha.1",
		"1.1.1-alpha.1+build.123": "1.1.2-alpha.1+build.123",
	}

	for original, expected := range valid {
		semver := MustParse(original)
		semver.IncrementPatch()
		assert.Equal(t, expected, semver.Format(), "Invalid PATCH version increment")
	}
}
