module gitlab.com/joe.brutto/versionator

go 1.16

require (
	github.com/alecthomas/kong v0.2.18
	github.com/pwaller/goupx v0.0.0-20160623083017-1d58e01d5ce2 // indirect
	github.com/stretchr/testify v1.7.0
)
