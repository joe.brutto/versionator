# Versionator

[![pipeline status](https://gitlab.com/joe.brutto/versionator/badges/master/pipeline.svg)](https://gitlab.com/joe.brutto/versionator/-/pipelines)

![Versionator][logo]

Simple, low-dependency executable for working with [Semantic Versioning](https://semver.org). This is similar to the
projects like [Python Semantic Release](https://python-semantic-release.readthedocs.io/en/latest/) and its inspiration
[semantic-release](https://github.com/semantic-release/semantic-release), but written in [Go](https://golang.org) just
so it's a single executable that can be added to build routines in [Docker](https://docker.com) and other CI/CD setups
without having to add large amounts of other dependencies. We tried to keep the binary as small as possible and keep
things extremely simple.

[[_TOC_]]

## Current Status

**Alpha** release.

I am using this for my own projects but it is not ready for a full release just yet.

## Sample Usage

### Validate

TBD

### Peek

TBD

### Increment

TBD

## TODO

  * add a version command from the CLI that just shows the version and exits
  * add support for versions stored in JSON, YAML, etc. files
  * add better documentation on the version of Semantic Versioning that we are supporting
  * CHANGELOG generation for our own builds, etc.
  * better error handling
  * implement less than and greater than comparators
  * add support for working with pre-release and build
  * more/better unit testing
  * add a bunch of static analysis things to the build
  * lock down branching/tagging strategy
  * build a better CI/CD Pipeline that is more GitLab idiomatic

[logo]: ./versionator-200x200.png
