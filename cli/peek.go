package cli

import (
	"github.com/alecthomas/kong"
	"gitlab.com/joe.brutto/versionator/semver"
)

// Peek defines the CLI command for us to peek at a value in a file.
type Peek struct {
	Increment string `required:"" short:"i" enum:"major,minor,patch" help:"The part of the Semantic Version to increment (major, minor or patch)"`
	Quiet     bool   `short:"q" help:"Do operations quietly (silence all output); just validates the file"`
	Path      string `arg:"" required:"" name:"path" help:"Path to the file to read from" type:"path"`
}

// Run implements the appropriate method conforming to Kong's parsing that kicks off the processing of the command.
func (p *Peek) Run(_ *kong.Context) error {
	// parsing
	parsedVersion, err := semver.ParseTextFile(p.Path)
	if err != nil {
		return err
	}
	incrementSpecification := semver.IncrementSpecificationFor(p.Increment)

	// do it & exit
	semver.Peek(parsedVersion, incrementSpecification, p.Quiet)
	return nil
}
