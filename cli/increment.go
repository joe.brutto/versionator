package cli

import (
	"fmt"
	"github.com/alecthomas/kong"
	"gitlab.com/joe.brutto/versionator/semver"
	"gitlab.com/joe.brutto/versionator/utility"
	"os"
)

// Increment defines the CLI command for us to increment a value in a file.
type Increment struct {
	Increment string `required:"" short:"i" enum:"major,minor,patch" help:"The part of the Semantic Version to increment (major, minor or patch)"`
	Peek      bool   `short:"p" help:"Alias for using the \"peek\" command we provide"`
	Quiet     bool   `short:"q" help:"Do operations quietly (silence all output)"`
	Path      string `arg:"" required:"" name:"path" help:"Path to the file to read from" type:"path"`
}

// Run implements the appropriate method conforming to Kong's parsing that kicks off the processing of the command.
func (i *Increment) Run(ctx *kong.Context) error {
	// "alias" the "peek" command
	if i.Peek {
		peekInstance := Peek{
			Increment: i.Increment,
			Quiet:     i.Quiet,
			Path:      i.Path,
		}
		return peekInstance.Run(ctx)
	}

	// pull the data
	parsedVersion, err := semver.ParseTextFile(i.Path)
	if err != nil {
		panic(err)
	}
	original := parsedVersion.Format()

	// do it
	incrementSpecification := semver.IncrementSpecificationFor(i.Increment)
	parsedVersion.Increment(incrementSpecification)
	newVersion := parsedVersion.Format()

	// print to the command line
	if !i.Quiet {
		fmt.Printf("\033[93mPrevious Version:\033[0m  %s\n", original)
		fmt.Printf("\033[92mNew Version:\033[0m       %s\n", newVersion)
	}

	// write the data to file
	file, err := os.Create(i.Path)
	if err != nil {
		panic(err)
	}
	defer utility.CloseQuietly(file)

	_, err = file.WriteString(newVersion)
	if err != nil {
		panic(err)
	}

	// exit
	return nil
}
