package cli

// Cli is the primary structure that defines the command line arguments that we support in the application.
type Cli struct {
	Peek      Peek      `cmd:"" help:"Peek to see what the next version will be"`
	Validate  Validate  `cmd:"" help:"Validates data in a file and returns an appropriate exit code"`
	Increment Increment `cmd:"" help:"Increment to a next version in a file "`
	Version   Version   `cmd:"" help:"Prints the application version and exits"`
}
