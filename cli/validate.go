package cli

import (
	"bufio"
	"fmt"
	"github.com/alecthomas/kong"
	"gitlab.com/joe.brutto/versionator/semver"
	"gitlab.com/joe.brutto/versionator/utility"
	"os"
	"strings"
)

// Validate defines the CLI command for us to validate data in a file.
type Validate struct {
	Zero  bool   `short:"z" help:"Regardless of status return a 0 (zero) exit code"`
	Quiet bool   `short:"q" help:"Do operations quietly (silence all output); just validates the file"`
	Path  string `arg:"" required:"" name:"path" help:"Path to the file to read from" type:"path"`
}

// Run implements the appropriate method conforming to Kong's parsing that kicks off the processing of the command.
func (v *Validate) Run(ctx *kong.Context) error {
	// read the data from file
	file, err := os.OpenFile(v.Path, os.O_RDONLY, 0)
	if err != nil {
		return err
	}
	defer utility.CloseQuietly(file)

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	content := strings.TrimSpace(scanner.Text())

	// TODO: move to semver package with its own, dedicated file
	// check to see if the content is valid
	if semver.Valid(content) {
		if !v.Quiet {
			printValidateSuccess(content)
		}
	} else {
		if !v.Quiet {
			printValidateFailure(content)
		}
		if !v.Zero {
			os.Exit(1)
		}
	}

	// exit
	return nil
}

// printValidateSuccess prints a standard validation success message.
func printValidateSuccess(input string) {
	fmt.Printf("\033[1m%s\033[0m is a \033[92mvalid\033[0m Semantic Version", input)
}

// printValidateFailure prints a standard validation failure message.
func printValidateFailure(input string) {
	fmt.Printf("\033[1m%s\033[0m is an \033[91minvalid\033[0m Semantic Version", input)
}
