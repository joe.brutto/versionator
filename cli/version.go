package cli

import (
	"fmt"
	"github.com/alecthomas/kong"
)

// Version defines the CLI command that prints the version of the application and exits.
type Version struct {
}

// Run implements the appropriate method conforming to Kong's parsing that kicks off the processing of the command.
func (v *Version) Run(_ *kong.Context) error {
	fmt.Println("TODO: implement this")
	return nil
}
